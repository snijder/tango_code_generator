import yaml

class device_template:
	"""
	List of all useful attributes
	"""

	def __init__(self, data, board=""):
		for k, v in data.items():
			print("{}: {}".format(k, v))

		self.instances = 1

		self.name = "{}".format(data["var_name"]) 	# Required
		self.description = None
		self.RW = "RW" 		#TODO: currently assumes that no R/RW attribute means RW by default
		self.max = None
		self.min = None
		self.scale = None
		self.dtype = None
		self.min_alarm = None
		self.max_alarm = None
		self.min_warn = None
		self.max_warn = None
		self.label = None
		self.display_unit = None
		self.unit = None
		self.standard_unit = None
		self.format = None
		self.polling_period = 0
		self.deltaTime = None
		self.deltaValue = None
		self.max_dim_x = 1
		self.max_dim_x = 1
		self.set_func = None	 # "write_func_RW"
		self.get_func = None	 # "read_func_R" or "read_func_RW"

		self.board = board
		self.group = ""
		self.group_nr = -1

		if "var_description" in data:
			self.description = data["var_description"]
		if "var_max" in data:
			self.max = data["var_max"]
		if "var_min" in data:
			self.min = data["var_min"]
		if "var_R/W" in data:
			self.RW = data["var_R/W"]

		#available types and their names: https://pytango.readthedocs.io/en/stable/server_api/server.html#module-tango.server
		if "var_width" in data:
			if int(data["var_width"]) != 1:
				# self.width = data["var_width"]
				if "var_scale" in data:
					self.dtype = 'double'
					self.scale = data["var_scale"]
				else:
					self.dtype = 'int64'
			else:
				self.dtype = 'bool'
		else:
			self.dtype = 'int64'

class method_template:
	def __init__(self, method_name, board):
		self.name = method_name
		self.board = board

class yaml_reader:
	# these 3 parameters are of interest for finding the sub files.
	dev_child_param = "dev_children"  # indicates the items below are sub files.
	child_name = "child_name"  # name of the group
	child_file_name = "child_conf"  # name of the file ( does not contain file extension)
	variable_param = "Variables"
	method_param = "Methods"
	group_param = "dev_groups"

	def __init__(self) -> object:

		self.current_file = ""
		self.previous_file = []

		self.current_device = ""

		self.variable_list = []
		self.method_list = []

		# same as above but ordered in groups
		self.board_list = []
		self.groups = []
		self.variables_grouped = []
		self.methods_grouped = []

	def add_method(self, data):
		if "method_invisible" in data:
			# we dont care about invisible methods, they don't exist I THINK????
			return
		else:
			self.method_list.append(method_template(data["method_name"], self.current_device))

	def start_read(self):
		self.read_file("YAML_files/LTS_pypcc.yaml")
		self.group_boards()
		self.group_groups()
		self.log_groups()



	def read_file(self, file):
		if len(self.current_file) != "":
			self.previous_file.append(self.current_file)
		self.current_file = file

		with open(file) as f:
			yaml_data = yaml.load_all(f, Loader=yaml.FullLoader)

			for i in yaml_data:
				self.reading(i)
		self.current_file = self.previous_file[-1]
		self.previous_file.pop(-1)

	def reading(self, data, param=""):


		if isinstance(data, list):
			for i in data:
				self.reading(i, param)

		elif isinstance(data, dict):
			for k, v in data.items():
				self.reading(v, k)
			self.catch_param(param, data)
		else:
			self.catch_param(param, data)


	def log_data(self):
		file = open("output_devices.csv", "w")

		for i in self.variable_list:
			text = "\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\"\n".format(i.board, i.name, i.description, i.RW, i.max, i.min, i.scale, i.dtype)
			print(text)
			file.write(text)

		for i in self.method_list:
			print("method: {}, {}".format(i.board, i.name))


	def log_groups(self):
		for i in range(len(self.board_list)):
			print(">>>\t", self.board_list[i])

			for j in self.variables_grouped[i]:
				text = "\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\",\"{}\"".format(j.board, j.name, j.description, j.RW, j.max, j.min, j.scale, j.dtype)
				print("\t", text)

	def group_boards(self):
		for i in self.variable_list:
			if not i.board in self.board_list:
				print("addding {}".format(i.board))
				self.board_list.append(i.board)
				self.variables_grouped.append([])

			for j in range(len(self.board_list)):
				if i.board == self.board_list[j]:
					self.variables_grouped[j].append(i)

	def group_groups(self):
		print(self.groups)
		for i in self.groups:
			for j in self.board_list:
				if j in i:
					print(i, j)


	def compare_lists(self, lst1, lst2):
		matches = 0

		for i in lst1:
			if i in lst2:
				matches += 1

	def catch_param(self, key, data):
		# print("{}:".format(key), data)

		key = str(key)

		if key.find(self.dev_child_param) != -1:
			print("{}:".format(key), data, " - there are sub files that I should open")
			file_name = "YAML_files/{}.yaml".format(dict(data)["child_conf"])
			print("opening: ", file_name)


			if self.current_file.find("switch") != -1:
				print("\t >>>\t", data)
				self.current_device = data["child_name"]

			self.read_file(file_name)

		elif key.find(self.method_param) != -1:
			print("{}:".format(key), data, " I should probably generate methods from this")
			self.add_method(data)
		elif key.find(self.variable_param) != -1:
			print("{}:".format(key), data, " there are variable items below this")
			dev = device_template(data, self.current_device)
			self.variable_list.append(dev)
		elif key.find(self.method_param) != -1:
			print("{}:".format(key), data, " I should probably generate methods from this")
		elif key.find(self.group_param) != -1:
			print("{}:".format(key), data["group_members"], " these boards need to be grouped")
			self.groups.append(data["group_members"])
		else:
			pass
