


from yaml_reader import *

# from opcua import ua, uamethod, Server
#
# import sys
# import time
# import logging

# import numpy
# from tango import AttrQuality, AttrWriteType, DispLevel, DevState, DebugIt
# from tango.server import Device, attribute, command, pipe, device_property

# write_data = "test"
#
# class device_template:
# 	"""
# 	List of all useful attributes
# 	"""
#
# 	def __init__(self):
# 		pass
#
# 	def create_device_XMI(self, device):
# 		template = f"""
# 		<attributes name="{device.name}_{device.RW}" attType="Scalar" rwType="{device.RW}" displayLevel="OPERATOR" polledPeriod="{device.polling_period}" maxX="" maxY="" allocReadMember="true" isDynamic="false">
# 		<dataType xsi:type="pogoDsl:{device.dtype}"/>
# 		<changeEvent fire="false" libCheckCriteria="false"/>
# 		<archiveEvent fire="false" libCheckCriteria="false"/>
# 		<dataReadyEvent fire="false" libCheckCriteria="true"/>
# 		<status abstract="false" inherited="false" concrete="true" concreteHere="true"/>
# 		<properties description="{device.description}" label="{device.label}" unit="{device.unit}" standardUnit="{device.standard_unit}"
# 				displayUnit="{device.display_unit}" format="" maxValue="{device.max}" minValue="{device.min}" maxAlarm="{device.max_alarm}"
# 				minAlarm="{device.min_alarm}" maxWarning="{device.max_warn}" minWarning="{device.min_warn}" {device.deltaTime}="" {device.deltaValue}=""/>
# 		</attributes>
# 		"""
# 		return template
#
#
# class device_instance:
# 	def __init__(self, attr_key):
# 		self.attribute_mapping = {}
# 		self.attribute_key = attr_key
#
#
# 	def read_func_R(self):
# 		"""Return the LED_R attribute."""
#
# 		self.read_value = self.attribute_mapping["LED_R"].get_value()
# 		return self.read_value
#
# 	def read_func_RW(self):
#
# 		"""Return the LED_RW attribute."""
# 		self.read_RW = self.attribute_mapping["LED_RW"].get_value()
# 		return self.read_RW
#
# 	# PROTECTED REGION END #    //  RCUSCC.LED_RW_read
#
# 	def write_func_RW(self, value):
# 		# PROTECTED REGION ID(RCUSCC.LED_RW_write) ENABLED START #
# 		"""Set the LED_RW attribute."""
# 		self.attribute_mapping["LED_RW"].set_value(value)
# 		self.write_value_RW = value
# 	# PROTECTED REGION END #    //  RCUSCC.LED_RW_write


class generator:

	def __init__(self):
		self.the_data = yaml_reader()
		self.the_data.start_read()
		self.the_data.group_groups()
		self.the_data.log_data()

	#NOTE: does not contain the actual generator, just the code to read the YAML files.


stuff = generator()





